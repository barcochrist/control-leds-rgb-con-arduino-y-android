//Importamos la libreria para convertir todas a salidad PWM
#include <SoftPWM.h>
#include <SoftPWM_timer.h>

//Declaramos cada uno de los pin en una variable
#define RED 3
#define GREEN 5
#define BLUE 6

#define RED2 9
#define GREEN2 10
#define BLUE2 11

#define RED3 2
#define GREEN3 4
#define BLUE3 7

#define RED4 8
#define GREEN4 12
#define BLUE4 13

//Declaramos un vector con los numero de los pins para posteriormente asignarles el modo de salida y apagarlos
uint8_t leds[12] = {3, 5, 6, 2, 4, 7, 8, 9, 10, 11, 12, 13};


void setup() {

  Serial.begin(9600);
  while (!Serial) {
    ; //Esperar el puerto serial para conectar
  }
  
  //Iniciamos la libreria importada
  SoftPWMBegin();
  
  //Declaramos los pines como salidas y los apagamos todos
  for (int i = 0; i < 12; i++) {
    pinMode(leds[i], OUTPUT);
    digitalWrite(leds[i], HIGH);
  }
}

//Funcion para resetear arduino
void(*resetFunc) (void) = 0;

//Declaramos estas 3 variables para luego controlar el color de los leds
int redVal;
int blueVal;
int greenVal;

//declaramos la variable que se va a incrementar cada vez que se realize una ronda
int numeroRondas = 0;

//declaramos la variable que va a recibir el caso, el numero de rondas y el tiempo de cambio
int caso;
int rondas;
int delayTime;

void loop() {

  if (Serial.available() > 0) {
    //Lee el primer numero Entero valido de una cadana (1,2,3) en este caso rondas toma el valor de 1
    caso = Serial.parseInt();

    //Lee el segundo numero Entero valido de una cadana (1,2,3) en este caso rondas toma el valor de 2
    rondas = Serial.parseInt();

    //Lee el tercer numero Entero valido de una cadana (1,2,3) en este caso rondas toma el valor de 3
    delayTime = Serial.parseInt();
  }

  switch (caso) {
    case 1:
    //Este modo hace que todos los LEDS RGB alumbren del mismo color al tiempo
      //El numero 255 es OFF y el 0 es ON

      //Determina la velocidad del FADE ON y el FADE OFF
      SoftPWMSetFadeTime(ALL, delayTime * 100, delayTime * 100);

      while (numeroRondas < rondas) {
        numeroRondas ++;
        //Empieza en el color Rojo, el cual se inicializa en HIGH, es decir apagado
        //Pasa del color Rojo al azul paulatinamente
        redVal = 255;
        for ( int i = 0 ; i < 255 ; i ++ ) {
          redVal --;
          SoftPWMSet( RED, 255 - redVal );
          SoftPWMSet( BLUE, 255 - i );
          SoftPWMSet( RED2, 255 - redVal );
          SoftPWMSet( BLUE2, 255 - i );
          SoftPWMSet( RED3, 255 - redVal );
          SoftPWMSet( BLUE3, 255 - i );
          SoftPWMSet( RED4, 255 - redVal );
          SoftPWMSet( BLUE4, 255 - i );
          delay( delayTime );
          
          //Cuando se reciba una r por Serial arduino pasara a modo Reset y apaga los LEDS
          if (Serial.read() == 'r') {
            resetFunc();
          }
        }

        //Prosigue del color Azul al Verde paulatinamente
        blueVal = 255;
        for ( int i = 0 ; i < 255 ; i ++ ) {
          blueVal --;
          SoftPWMSet( BLUE, 255 - blueVal );
          SoftPWMSet( GREEN, 255 - i );
          SoftPWMSet( BLUE2, 255 - blueVal );
          SoftPWMSet( GREEN2, 255 - i );
          SoftPWMSet( BLUE3, 255 - blueVal );
          SoftPWMSet( GREEN3, 255 - i );
          SoftPWMSet( BLUE4, 255 - blueVal );
          SoftPWMSet( GREEN4, 255 - i );
          delay( delayTime );
          
          //Cuando se reciba una r por Serial arduino pasara a modo Reset y apaga los LEDS
          if (Serial.read() == 'r') {
           resetFunc();
          }
        }


        //Prosigue del color Verde al Rojo paulatinamente
        greenVal = 255;
        for ( int i = 0 ; i < 255 ; i ++ ) {
          greenVal --;
          SoftPWMSet( GREEN, 255 - greenVal );
          SoftPWMSet( RED, 255 - i );
          SoftPWMSet( GREEN2, 255 - greenVal );
          SoftPWMSet( RED2, 255 - i );
          SoftPWMSet( GREEN3, 255 - greenVal );
          SoftPWMSet( RED3, 255 - i );
          SoftPWMSet( GREEN4, 255 - greenVal );
          SoftPWMSet( RED4, 255 - i );
          delay( delayTime );
          
          //Cuando se reciba una r por Serial arduino pasara a modo Reset y apaga los LEDS
          if (Serial.read() == 'r') {
            resetFunc();
          }
        }
      }
      
      //Una vez finaliza las rondas resetea la variable rondas y apaga los leds RGB
      rondas = 0;
      numeroRondas = 0;

      //Apagar los LEDS
      for (int i = 0; i < 12; i++) {
        SoftPWMSet(leds[i], 255);
      }
      break;

    case 2:
    //Este modo hace que cada uno de los leds alumbren de un color diferente
      //El numero 255 es OFF y el 0 es ON

      //Determina la velocidad del FADE ON y el FADE OFF
      SoftPWMSetFadeTime(ALL, delayTime * 100, delayTime * 100);


      while (numeroRondas < rondas) {
        numeroRondas ++;

        redVal = 255;
        for ( int i = 0 ; i < 255 ; i ++ ) {
          redVal --;
          SoftPWMSet( RED, 255 - redVal );
          SoftPWMSet( BLUE, 255 - i );
          SoftPWMSet( BLUE2, 255 - redVal );
          SoftPWMSet( GREEN2, 255 - i );
          SoftPWMSet( GREEN3, 255 - redVal );
          SoftPWMSet( RED3, 255 - i );
          SoftPWMSet( RED4, 255 - redVal );
          SoftPWMSet( GREEN4, 255 - i );
          delay( delayTime );
          
          //Cuando se reciba una r por Serial arduino pasara a modo Reset apagando los LEDS RGB
          if (Serial.read() == 'r') {
           resetFunc();
          }
        }

        blueVal = 255;
        for ( int i = 0 ; i < 255 ; i ++ ) {
          blueVal --;
          SoftPWMSet( BLUE, 255 - blueVal );
          SoftPWMSet( GREEN, 255 - i );
          SoftPWMSet( GREEN2, 255 - blueVal );
          SoftPWMSet( RED2, 255 - i );
          SoftPWMSet( RED3, 255 - blueVal );
          SoftPWMSet( BLUE3, 255 - i );
          SoftPWMSet( GREEN4, 255 - blueVal );
          SoftPWMSet( BLUE4, 255 - i );
          delay( delayTime );
          
          //Cuando se reciba una r por Serial arduino pasara a modo Reset 
          //Apagando los LEDS
          if (Serial.read() == 'r') {
           resetFunc();
          }
        }

        //Prosigue del color Verde al Rojo paulatinamente
        greenVal = 255;
        for ( int i = 0 ; i < 255 ; i ++ ) {
          greenVal --;
          SoftPWMSet( GREEN, 255 - greenVal );
          SoftPWMSet( RED, 255 - i );
          SoftPWMSet( RED2, 255 - greenVal );
          SoftPWMSet( BLUE2, 255 - i );
          SoftPWMSet( BLUE3, 255 - greenVal );
          SoftPWMSet( GREEN3, 255 - i );
          SoftPWMSet( BLUE4, 255 - greenVal );
          SoftPWMSet( RED4, 255 - i );
          delay( delayTime );
          
          //Cuando se reciba una r por Serial arduino pasara a modo Reset apagando los LEDS
          if (Serial.read() == 'r') {
           resetFunc();
          }
        }
      }
      
      //Una vez finaliza las rondas resetea la variable rondas y apaga los leds RGB
      //Resetear las rondas
      rondas = 0;
      numeroRondas = 0;

      //Apagar los LEDS
      for (int i = 0; i < 12; i++) {
        SoftPWMSet(leds[i], 255);
      }
      break;
  }
}

