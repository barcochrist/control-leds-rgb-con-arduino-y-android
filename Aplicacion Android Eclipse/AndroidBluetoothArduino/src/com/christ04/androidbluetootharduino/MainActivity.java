package com.christ04.androidbluetootharduino;



import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {

	BluetoothUtils bluetooth;
	EditText repeticiones, velocidad;
	String rep, vel;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        repeticiones = (EditText) findViewById(R.id.editTextRepeticiones);
        velocidad = (EditText) findViewById(R.id.editTextVelocidad);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void conectarBluetooth(View v)  {
    	
    	Intent i = new Intent(MainActivity.this,ConectarActivity.class);
		startActivity(i);
    	
	}
    
    public void enviarModo1(View v) throws UnsupportedEncodingException {
		// byte[] data = (""+1+","+2+","+10);
    	rep = repeticiones.getText().toString();
		vel = velocidad.getText().toString();
		
    	bluetooth = ConectarActivity.bluetooth;
		byte[] data = ("" + 1 + "," + rep + "," + vel).getBytes("US-ASCII");

		// Si no estamos conectados, terminamos
		if (bluetooth.isConnected() == false) {
			Toast.makeText(this, "Conectar Primero", Toast.LENGTH_SHORT).show();
			return;
		}

		bluetooth.send(data);
	}
    

	public void enviarModo2(View v) throws UnsupportedEncodingException {
		rep = repeticiones.getText().toString();
		vel = velocidad.getText().toString();
		
		bluetooth = ConectarActivity.bluetooth;
		
		byte[] data2 = ("" + 2 + "," + rep + "," + vel).getBytes("US-ASCII");
		// Si no estamos conectados, terminamos
		if (bluetooth.isConnected() == false) {
			Toast.makeText(this, "Conectar Primero", Toast.LENGTH_SHORT).show();
			return;
		}
		bluetooth.send(data2);
	}
	
	public void enviarReset(View v) throws UnsupportedEncodingException {
	
		bluetooth = ConectarActivity.bluetooth;
		
		byte[] data3 = ("r").getBytes("US-ASCII");
		// Si no estamos conectados, terminamos
		if (bluetooth.isConnected() == false) {
			Toast.makeText(this, "Conectar Primero", Toast.LENGTH_SHORT).show();
			return;
		}
		bluetooth.send(data3);
	}
}
