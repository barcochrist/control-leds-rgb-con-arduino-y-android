package com.christ04.androidbluetootharduino;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ConectarActivity extends Activity implements OnItemClickListener {

	static BluetoothUtils bluetooth;
	private ListView dispostivios;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conectar);

		// para simplificar us uso
		bluetooth = new BluetoothUtils();

		// Obtenemos la lista de elementos
		dispostivios = (ListView) findViewById(R.id.dispositivos);

		// Obtenemos los nombres de los dispositivos
		// bluetooth vinculados
		String[] nombres = bluetooth.getNames();

		// Asignamos los nombres a la lista
		dispostivios.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, nombres));

		// Leemos los "clicks" sobre los elementos de
		// la lista
		dispostivios.setOnItemClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conectar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if (bluetooth.connect(arg2))
			Toast.makeText(this, "conectado", Toast.LENGTH_SHORT).show();
		Intent i = new Intent(ConectarActivity.this,MainActivity.class);
		startActivity(i);

	}

	
}
